ENV['RACK_ENV'] ||= 'test'
require './config/environment'

RSpec::Expectations.configuration.warn_about_potential_false_positives = false
RSpec.configure do |config|
  config.color = true
  config.after :each do
    ActiveRecord::Base.subclasses.each(&:delete_all)
  end
end
