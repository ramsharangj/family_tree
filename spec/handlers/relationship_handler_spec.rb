require_relative '../spec_helper.rb'

describe RelationshipHandler do
  describe '#meet_family' do
    it 'returns the members when relationship and name is given' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      father.add_child(name: 'Great Khali', sex: 'male')
      father_brother = Member.create!(name: 'Scott', sex: 'male', depth: 1)
      member = Member.find_by(name: 'Great Khali')
      results = meet_family(member.name, 'uncles')
      expect(results.first).to eq father_brother
    end

    it 'raises an error when the relationship is invalid' do
      member = Member.create!(name: 'John', sex: 'male', depth: 1)
      expect { meet_family(member.name, 'useless') }.to raise_error
    end
  end

  describe '#add_new_born' do
    it 'adds a new born to the member when name of the parent and the name and sex of the child is given' do
      member = Member.create!(name: 'John', sex: 'male', depth: 1)
      add_new_born(member.name, 'Zara', 'female')
      expect(member.children.first.name).to eq 'Zara'
    end
  end

  describe '#maximum_daughters' do
    it 'returns the mother with the most daughters' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      father.add_child(name: 'Great Khali', sex: 'male')
      father.add_child(name: 'Great Durga', sex: 'female')
      father.add_child(name: 'Greater Durga', sex: 'female')
      father1 = Member.create!(name: 'Steven', sex: 'male', depth: 1)
      father1.add_spouse(name: 'Alex', sex: 'female')
      father1.add_child(name: 'Romeo', sex: 'male')
      father1.add_child(name: 'Haley', sex: 'female')
      mother_with_most_daughters = Member.find_by(name: 'Anna')
      result = maximum_daughters
      expect(result).to eq mother_with_most_daughters
    end
  end
end
