require_relative '../spec_helper.rb'

describe Member do
  it 'consists of name and sex' do
    member = Member.create!(name: 'John', sex: 'male', depth: 1)
    expect(member.name).to eq 'John'
  end

  describe '#add_child' do
    it 'adds children to a parent' do
      parent = Member.create!(name: 'John', sex: 'male', depth: 1)
      parent.add_child(name: 'Doe', sex: 'male')
      expect(parent.children.count).to eq 1
      expect(parent.children.first.name).to eq 'Doe'
    end

    it 'does not add children to a parent if name or sex is not passed' do
      parent = Member.create!(name: 'John', sex: 'male', depth: 1)
      expect { parent.add_child(name: 'Doe') }.to raise_error
    end
  end

  describe '#add_spouse' do
    it 'adds spouse to a member' do
      member = Member.create!(name: 'John', sex: 'male', depth: 1)
      member.add_spouse(name: 'Anna', sex: 'female')
      expect(member.spouse.name).to eq 'Anna'
      expect(member.spouse.depth).to eq member.depth
    end

    it 'not add a spouse if there is no name and sex passed' do
      member = Member.create!(name: 'John', sex: 'male', depth: 1)
      expect { member.add_spouse(name: 'Anna') }.to raise_error
    end
  end

  describe '#parents' do
    it 'returns the parents of the member' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      father.add_child(name: 'Great Khali', sex: 'male')
      member = Member.find_by(name: 'Great Khali')
      expect(member.parents[:father]).to eq father
      expect(member.parents[:mother]).to eq father.spouse
    end

    it 'returns only father if no spouse and child is adopted' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_child(name: 'Great Khali', sex: 'male')
      member = Member.find_by(name: 'Great Khali')
      expect(member.parents[:father]).to eq father
      expect(member.parents[:mother]).to be_nil
    end
  end

  describe '#sons' do
    it 'return the sons of the member' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      father.add_child(name: 'Great Khali', sex: 'male')
      expect(father.sons.first.name).to eq 'Great Khali'
    end

    it 'return the sons of the member and spouse' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      mother = Member.find_by(name: 'Anna')
      mother.add_child(name: 'Greater Anna', sex: 'male')
      father.add_child(name: 'Great Khali', sex: 'male')
      expect(father.sons.count).to eq 2
      expect(father.sons.map(&:name)).to include 'Great Khali'
    end
  end

  describe '#daughters' do
    it 'return the daughters of the member' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      father.add_child(name: 'Great Khali', sex: 'male')
      father.add_child(name: 'Great Durga', sex: 'female')
      father.add_child(name: 'Greater Durga', sex: 'female')
      expect(father.daughters.count).to eq 2
      expect(father.daughters.map(&:name)).to include 'Great Durga'
    end

    it 'return the daughters of the member and spouse' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      mother = Member.find_by(name: 'Anna')
      mother.add_child(name: 'Greater Anna', sex: 'female')
      father.add_child(name: 'Great Khali', sex: 'female')
      expect(father.daughters.count).to eq 2
      expect(father.daughters.map(&:name)).to include 'Greater Anna'
    end
  end

  describe '#brothers' do
    it 'returns the brothers of them member' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      mother = Member.find_by(name: 'Anna')
      father.add_child(name: 'Great Khali', sex: 'male')
      father.add_child(name: 'Great Durga', sex: 'female')
      father.add_child(name: 'Greater Durga', sex: 'female')
      mother.add_child(name: 'Greater Anna', sex: 'female')
      member = Member.find_by(name: 'Greater Durga')
      expect(member.brothers.first.name).to eq 'Great Khali'
    end
  end

  describe '#sisters' do
    it 'returns the sisters of them member' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      mother = Member.find_by(name: 'Anna')
      father.add_child(name: 'Great Khali', sex: 'male')
      father.add_child(name: 'Great Durga', sex: 'female')
      father.add_child(name: 'Greater Durga', sex: 'female')
      mother.add_child(name: 'Greater Anna', sex: 'female')
      member = Member.find_by(name: 'Greater Durga')
      expect(member.sisters.count).to eq 2
      expect(member.sisters.map(&:name)).to include 'Great Durga'
    end
  end

  describe '#grand_daughters' do
    it 'returns the grand daughters of the member' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      mother = Member.find_by(name: 'Anna')
      father.add_child(name: 'Great Khali', sex: 'male')
      father.add_child(name: 'Great Durga', sex: 'female')
      father.add_child(name: 'Greater Durga', sex: 'female')
      mother.add_child(name: 'Greater Anna', sex: 'female')
      member = Member.find_by(name: 'Great Durga')
      member.add_spouse(name: 'Steven', sex: 'male')
      member.add_child(name: 'Gerrard', sex: 'female')
      member1 = Member.find_by(name: 'Greater Durga')
      member1.add_spouse(name: 'Mark', sex: 'male')
      member1.add_child(name: 'Mahon', sex: 'female')
      member1.add_child(name: 'Alex', sex: 'male')
      expect(father.grand_daughters.count).to eq 2
      expect(father.grand_daughters.map(&:name)).to include 'Gerrard'
    end
  end

  describe '#cousins' do
    it 'returns the cousins of the member' do
      member = Member.create!(name: 'John', sex: 'male', depth: 1)
      brother = Member.create!(name: 'Paul', sex: 'male', depth: 1)
      member.add_spouse(name: 'Anna', sex: 'female')
      brother.add_spouse(name: 'Maria', sex: 'female')
      brother.add_child(name: 'Paul Maria', sex: 'female')
      sister = Member.create!(name: 'Sister', sex: 'female', depth: 1)
      sister.add_spouse(name: 'Kevin', sex: 'male')
      Member.find_by(name: 'Kevin').add_child(name: 'cousin', sex: 'male')
      person = member.add_child(name: 'Collins John', sex: 'male')
      expect(person.cousins.count).to eq 2
      expect(person.cousins.map(&:name)).to include 'cousin'
    end
  end

  describe '#brother_in_law' do
    it 'returns the brother in law of the member' do
      head = Member.create!(name: 'John', sex: 'male', depth: 1)
      head.add_spouse(name: 'Anna', sex: 'female')
      head.add_child(name: 'John 2', sex: 'male')
      head.add_child(name: 'John 3', sex: 'male')
      head.add_child(name: 'Anna 1', sex: 'female')
      Member.find_by(name: 'Anna 1').add_spouse(name: 'Bill', sex: 'male')
      member = Member.find_by(name: 'John 2')
      expect(member.brother_in_laws.count).to eq 1
      expect(member.brother_in_laws.first.name).to eq 'Bill'
    end
  end

  describe '#sister_in_law' do
    it 'returns the brother in law of the member' do
      head = Member.create!(name: 'John', sex: 'male', depth: 1)
      head.add_spouse(name: 'Anna', sex: 'female')
      head.add_child(name: 'John 2', sex: 'male')
      head.add_child(name: 'John 3', sex: 'female')
      head.add_child(name: 'Anna 1', sex: 'female')
      Member.find_by(name: 'Anna 1').add_spouse(name: 'Bill', sex: 'male')
      member = Member.find_by(name: 'Bill')
      expect(member.sister_in_laws.count).to eq 1
      expect(member.sister_in_laws.map(&:name)).to include 'John 3'
    end
  end

  describe '#aunts' do
    it 'returns all the aunts of the memeber' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      father.add_child(name: 'Great Khali', sex: 'male')
      father_sister = Member.create!(name: 'Jesse', sex: 'female', depth: 1)
      member = Member.find_by(name: 'Great Khali')
      expect(member.aunts.count).to eq 1
      expect(member.aunts.first).to eq father_sister
    end
  end

  describe '#uncles' do
    it 'returns all the uncles of the memeber' do
      father = Member.create!(name: 'John', sex: 'male', depth: 1)
      father.add_spouse(name: 'Anna', sex: 'female')
      father.add_child(name: 'Great Khali', sex: 'male')
      father_brother = Member.create!(name: 'Scott', sex: 'male', depth: 1)
      member = Member.find_by(name: 'Great Khali')
      expect(member.uncles.count).to eq 1
      expect(member.uncles.first).to eq father_brother
    end
  end
end
