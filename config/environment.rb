require 'bundler'
Bundler.require
require 'active_record'
require 'yaml'
require 'sqlite3'
require 'byebug'

require_relative '../models/member'

require_relative '../handlers/relationship_handler'

include RelationshipHandler
db_options = YAML.load(File.read('./config/database.yml'))[ENV['RACK_ENV']]
ActiveRecord::Base.establish_connection(db_options)
