class Member < ActiveRecord::Base
  has_many :children, class_name: "Member", foreign_key: "parent_id"
  belongs_to :parent, class_name: "Member"
  has_one :spouse, class_name: "Member", foreign_key: "spouse_id"
  belongs_to :spouse, class_name: "Member"

  validates :name, :sex, :depth, presence: true

  def add_child(options={})
    Member.create!(name: options[:name], sex: options[:sex], depth: self.depth + 1, parent: self)
  rescue ActiveRecord::RecordInvalid => e
    raise "Record not created because of #{e}"
  end

  def add_spouse(options={})
    Member.create!(name: options[:name], sex: options[:sex], depth: self.depth, spouse: self)
  rescue ActiveRecord::RecordInvalid => e
    raise "Record not created because of #{e}"
  end

  def parents
    array = [self.parent, self.parent.spouse].compact
    {
      father: array.detect { |ele| ele.sex == "male" },
      mother: array.detect { |ele| ele.sex == "female" }
    }
  end

  def all_children
    Member.where(parent_id: [self.id, self.spouse.id])
  end

  def all_siblings
    Member.where(parent_id: [self.parent.id, self.parent.spouse.id]).where("id != ?", self.id)
  end

  def sons
    all_children.where(sex: 'male')
  end

  def daughters
    all_children.where(sex: 'female')
  end

  def brothers
    all_siblings.where(sex: 'male')
  end

  def sisters
    all_siblings.where(sex: 'female')
  end

  def grand_daughters
    Member.where(parent_id: all_children.map(&:id)).where(sex: 'female')
  end

  def parents_siblings
    Member.includes(:children).where(depth: self.parent.depth).where.not(id: [self.parent_id, self.parent.spouse_id])
  end

  def cousins
    if parents_siblings.count > 1
      Member.where(parent_id: parents_siblings.where.not(children_members: {id: nil}).map(&:id))
    else
      []
    end
  end

  def brother_in_laws
    if self.parent_id
      Member.where('parent_id NOT IN (?) OR parent_id is NULL', [self.parent_id, self.parent.spouse_id]).where(sex: 'male', depth: self.depth)
    else
      Member.where(parent_id: [self.spouse.parent_id, self.spouse.parent.spouse_id]).where.not(id: self.spouse_id).where(sex: 'male', depth: self.depth)
    end
  end

  def sister_in_laws
    if self.parent_id
      Member.where('parent_id NOT IN (?) OR parent_id is NULL', [self.parent_id, self.parent.spouse_id]).where(sex: 'female', depth: self.depth)
    else
      Member.where(parent_id: [self.spouse.parent_id, self.spouse.parent.spouse_id]).where.not(id: self.spouse_id).where(sex: 'female', depth: self.depth)
    end
  end

  def aunts
    parents_siblings.where(sex: 'female')
  end

  def uncles
    parents_siblings.where(sex: 'male')
  end
end
