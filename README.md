# Requirements for the project

* Ruby
* Bundler

# Setting up the project

* Run `bundle install`
* Run `bundle exec rspec` for running tests
* Run `ruby application.rb` to run the application with the needed methods.
* Run `irb -r './application.rb'` to use the methods defined.
