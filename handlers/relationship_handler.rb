module RelationshipHandler
  def meet_family(name, relationship)
    member = Member.find_by(name: name)
    member.send(relationship)
  rescue => e
    raise "Error occurred: #{e}"
  end

  def add_new_born(parent_name, child_name, child_sex)
    member = Member.find_by(name: parent_name)
    member.add_child(name: child_name, sex: child_sex)
  rescue => e
    raise "Error occurred: #{e}"
  end

  def maximum_daughters
    member = Member.includes(:children).where.not(children_members: {id: nil}).sort_by { |mem| mem.daughters.count }
    member = member.last if member.count > 0
    [member, member.spouse].detect { |mem| mem.sex == 'female' }
  rescue => e
    raise "Error occurred: #{e}"
  end
end
