class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :name, null: false
      t.string :sex, null: false
      t.references :children, index: true
      t.references :parent, index: true
      t.references :spouse, index: true
      t.integer :depth, null: false
      t.timestamps null: false
    end
  end
end
